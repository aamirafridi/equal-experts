```
yarn
```

To run the server:

```
yarn dev
```

URL: http://localhost:3001/grocery-list (Methods: GET, POST, PUT, DELETE)
