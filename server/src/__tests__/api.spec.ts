import app from "../api";
import request from "supertest";

describe("API endpoints", () => {
  it("should add new grocery item", async () => {
    const res = await request(app)
      .post("/grocery-list")
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .send(
        JSON.stringify({
          label: "apples",
          quantity: 1
        })
      );
    expect(res.status).toEqual(200);
    expect(res.body).toEqual([
      expect.objectContaining({
        label: "apples",
        quantity: 1
      })
    ]);
  });

  it("should get grocery list", async () => {
    await request(app)
      .post("/grocery-list")
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .send(
        JSON.stringify({
          label: "oranges",
          quantity: 2
        })
      );

    const res = await request(app).get("/grocery-list");
    expect(res.status).toEqual(200);
    expect(res.body).toEqual([
      expect.objectContaining({
        label: "oranges",
        quantity: 2
      }),
      expect.objectContaining({
        label: "apples",
        quantity: 1
      })
    ]);
  });

  it("should mark grocery item as bought/done", async () => {
    const { body } = await request(app).get("/grocery-list");
    const itemToUpdate = body[0];

    const res = await request(app).put(`/grocery-list/${itemToUpdate.id}`);
    expect(res.status).toEqual(200);
    expect(res.body).toEqual([
      expect.objectContaining({
        label: "apples",
        quantity: 1
      }),
      expect.objectContaining({
        label: "oranges",
        quantity: 2,
        isBought: true
      })
    ]);
  });

  it("should delete grocery item", async () => {
    const { body } = await request(app).get("/grocery-list");
    const itemToUpdate = body[0];

    const res = await request(app).delete(`/grocery-list/${itemToUpdate.id}`);
    expect(res.status).toEqual(200);
    expect(res.body).toEqual([
      expect.objectContaining({
        label: "oranges",
        quantity: 2
      })
    ]);
  });
});
