import express, { Application, Response } from "express";
import bodyParser from "body-parser";
import shortid from "shortid";

const FileSync = require("lowdb/adapters/FileSync");
const Memory = require("lowdb/adapters/Memory");
const low = require("lowdb");

const db = low(
  process.env.NODE_ENV === "test" ? new Memory() : new FileSync("db.json")
);

db.defaults({ grocery: [] }).write();

const app: Application = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.all("*", function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
  res.header("Access-Control-Allow-Headers", "Content-Type");
  next();
});

const getSortedList = () =>
  db
    .get("grocery")
    .sortBy("isBought")
    .reverse()
    .value();

app.get("/grocery-list", function(_req, res) {
  return res.status(200).json(getSortedList());
});

app.post("/grocery-list", function(req, res) {
  const { label, quantity } = req.body;
  db.get("grocery")
    .push({ id: shortid.generate(), label, quantity })
    .write();
  return res.status(200).json(getSortedList());
});

app.put("/grocery-list/:itemId", function(req, res: Response) {
  const { itemId: id } = req.params;
  db.get("grocery")
    .find({ id })
    .assign({ isBought: true })
    .write();

  return res.status(200).json(getSortedList());
});

app.delete("/grocery-list/:itemId", function(req, res: Response) {
  const { itemId: id } = req.params;
  db.get("grocery")
    .remove({ id })
    .write();

  return res.status(200).json(getSortedList());
});

app.listen(3001, function() {
  console.log("Grocery app listening on port 3001");
});

export default app;
