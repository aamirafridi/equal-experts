# Equal Experts grocery list test

- **Version**: 04fa1404c2b3b7be0e5909d4ba93d1332bc76e98
- **Time spent**: ~2hrs
- **API**: `expressjs`, `typescript`, `lowDB` and for testing `jest` with `supertest` [TDD]
- **APP**: `React` with `react-create-app`, `typescript`, `react hooks` and for testing `jest` with `react-testing-library` [TDD]
- **Process**: Started with API tests, followed by implementation. Then worked on react app by writing tests using react-testing-library and finally styling with plain css

---

### Steps to run the app

```bash
# get code
git clone git@gitlab.com:aamirafridi/equal-experts.git

# api
cd server
yarn
yarn test
yarn test --coverage
yarn dev

# app
cd app
yarn
yarn test
yarn test-coverage
yarn start
```

### Preview

![image](https://user-images.githubusercontent.com/55896/74594292-7500ef00-502c-11ea-8cf8-239e38986245.png)
