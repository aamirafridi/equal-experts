import React from "react";
import {
  fireEvent,
  render,
  wait,
  waitForElementToBeRemoved,
  waitForElement,
  act
} from "@testing-library/react";
import { FetchMock } from "jest-fetch-mock";

import App from "./App";

describe("Grocery App", () => {
  beforeEach(() => {
    jest.restoreAllMocks();
    (fetch as FetchMock).resetMocks();
  });

  it("should shows loader while fetching the grocery list", () => {
    const { getByText } = render(<App />);
    const loader = getByText(/Loading/);
    expect(loader).toBeInTheDocument();
  });

  it("should render empty list message", async () => {
    (window.fetch as jest.Mock).mockResolvedValueOnce({
      status: 200,
      json: () => []
    });
    const { getByText } = render(<App />);
    await waitForElementToBeRemoved(() => getByText(/Loading/));
    expect(getByText(/No items found/)).toBeInTheDocument();
  });

  it("should render the grocery list", async () => {
    (window.fetch as jest.Mock).mockResolvedValueOnce({
      status: 200,
      json: () => [
        { id: 1, label: "apples", quantity: "1" },
        { id: 2, label: "oranges", quantity: "1" }
      ]
    });
    const { getByText } = render(<App />);
    await waitForElementToBeRemoved(() => getByText(/Loading/));
    expect(getByText(/apples/)).toBeInTheDocument();
    expect(getByText(/oranges/)).toBeInTheDocument();
  });

  it("should add new item to the grocery list", async () => {
    const { getByText, getByLabelText, getByTitle } = render(<App />);
    await waitForElementToBeRemoved(() => getByText(/Loading/));

    const alertSpy = jest.spyOn(window, "alert").mockImplementation(str => str);

    act(() => {
      fireEvent.click(getByTitle(/Add item/));
    });

    expect(alertSpy).toHaveBeenCalled();

    act(() => {
      fireEvent.change(getByLabelText(/Label/), {
        target: { value: "mangos", name: "label" }
      });
    });
    act(() => {
      fireEvent.change(getByLabelText(/Quantity/), {
        target: { value: "1", name: "quantity" }
      });
    });
    act(() => {
      fireEvent.click(getByTitle(/Add item/));
    });

    await wait(() => {
      expect(window.fetch).toHaveBeenLastCalledWith(
        "http://localhost:3001/grocery-list",
        expect.objectContaining({
          body: JSON.stringify({
            label: "mangos",
            quantity: "1"
          }),
          method: "POST"
        })
      );
    });
  });

  it("should mark item as bought", async () => {
    (window.fetch as jest.Mock).mockResolvedValueOnce({
      status: 200,
      json: () => [{ id: 1, label: "apples", quantity: "1" }]
    });

    const { getByText, getByAltText } = render(<App />);
    await waitForElementToBeRemoved(() => getByText(/Loading/));
    act(() => {
      fireEvent.click(getByAltText(/mark as bought/));
    });

    await wait(() => {
      expect(window.fetch).toBeCalledWith(
        "http://localhost:3001/grocery-list/1",
        expect.objectContaining({
          method: "PUT"
        })
      );
    });
  });

  it("should delete item", async () => {
    (window.fetch as jest.Mock).mockResolvedValueOnce({
      status: 200,
      json: () => [{ id: 1, label: "apples", quantity: "1" }]
    });
    jest.spyOn(window, "confirm").mockImplementation(() => true);

    const { getByText, getByAltText } = render(<App />);
    await waitForElementToBeRemoved(() => getByText(/Loading/));
    act(() => {
      fireEvent.click(getByAltText(/delete/));
    });

    await wait(() => {
      expect(window.fetch).toBeCalledWith(
        "http://localhost:3001/grocery-list/1",
        expect.objectContaining({
          method: "DELETE"
        })
      );
    });
  });

  it("should display error on api failure", async () => {
    (window.fetch as jest.Mock).mockRejectedValue({
      status: 400
    });
    const { getByText } = render(<App />);
    await waitForElement(() => getByText(/Ops there is something wrong/));
  });
});
