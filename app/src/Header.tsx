import React, { FunctionComponent, memo } from "react";

const Header: FunctionComponent = () => {
  return (
    <header className="header">
      <div className="container">
        <h1>
          <a href="https://www.equalexperts.com/">
            <img
              className="logo"
              src="https://www.equalexperts.com/wp-content/themes/equalexperts/assets/logo.svg"
              alt="[=] Equal Experts"
            />
          </a>
        </h1>
        <h2 className="app-name">Grocery list</h2>
      </div>
    </header>
  );
};

export default memo(Header);
