import React, {
  FunctionComponent,
  useEffect,
  useState,
  useMemo,
  useCallback
} from "react";
import "./App.css";
import Header from "./Header";
import tick from "./tick.svg";
import bin from "./bin.svg";

const apiUrl = "http://localhost:3001/grocery-list";

type GroceryItem = {
  id: string;
  label: string;
  quantity: string;
  isBought?: boolean;
};

const blankItem = { label: "", quantity: "" };

const App: FunctionComponent = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [submitFailed, setSubmitFailed] = useState(false);
  const [items, setItems] = useState<GroceryItem[]>([]);
  const [newItem, setNewItem] = useState(blankItem);

  useEffect(() => {
    async function getItems() {
      try {
        setIsLoading(true);
        setSubmitFailed(false);
        const response = await fetch(apiUrl);
        setItems(await response.json());
        setIsLoading(false);
      } catch {
        setIsLoading(false);
        setSubmitFailed(true);
      }
    }
    getItems();
  }, []);

  const handleSubmit = useCallback(
    async (event: React.FormEvent<HTMLFormElement>) => {
      event.preventDefault();
      if (
        newItem.label.trim().length === 0 ||
        newItem.quantity.trim().length === 0 ||
        isNaN(Number(newItem.quantity)) ||
        Number(newItem.quantity) < 1
      ) {
        return window.alert("Please enter item label and quantity");
      }
      try {
        setIsLoading(true);
        setSubmitFailed(false);
        const response = await fetch(apiUrl, {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify(newItem)
        });
        setItems(await response.json());
        setIsLoading(false);
        setNewItem(blankItem);
      } catch {
        setIsLoading(false);
        setSubmitFailed(true);
      }
    },
    [newItem]
  );

  const handleBought = useCallback(async (id: GroceryItem["id"]) => {
    try {
      setIsLoading(true);
      setSubmitFailed(false);
      const response = await fetch(`${apiUrl}/${id}`, {
        method: "PUT",
        headers: { "Content-Type": "application/json" }
      });
      setItems(await response.json());
      setIsLoading(false);
    } catch {
      setIsLoading(false);
      setSubmitFailed(true);
    }
  }, []);

  const handleDelete = useCallback(async (id: GroceryItem["id"]) => {
    try {
      setIsLoading(true);
      setSubmitFailed(false);
      const response = await fetch(`${apiUrl}/${id}`, {
        method: "DELETE",
        headers: { "Content-Type": "application/json" }
      });
      setItems(await response.json());
      setIsLoading(false);
    } catch {
      setIsLoading(false);
      setSubmitFailed(true);
    }
  }, []);

  const groceryList = useMemo(() => {
    if (items.length === 0) {
      return <div className="alert alert--info">No items found!</div>;
    }
    return (
      <ol>
        {items.map(item => {
          return (
            <li
              className="item"
              key={item.id}
              style={{
                textDecoration: item.isBought ? "line-through" : "none"
              }}
            >
              {item.label}{" "}
              <span title="Quantity" className="item__quantity">
                ({item.quantity})
              </span>
              <div className="item__actions">
                <button
                  onClick={e => handleBought(item.id)}
                  disabled={item.isBought}
                >
                  <img src={tick} alt="mark as bought" />
                </button>

                {
                  <button
                    onClick={e =>
                      window.confirm("Are you sure?") && handleDelete(item.id)
                    }
                  >
                    <img src={bin} alt="delete" />
                  </button>
                }
              </div>
            </li>
          );
        })}
      </ol>
    );
  }, [items, handleBought, handleDelete]);

  return (
    <div>
      <Header />
      {submitFailed && "Ops there is something wrong. Please try again"}
      <main>
        <div className="container">
          <form noValidate onSubmit={handleSubmit}>
            <label>
              <span className="sr-only">Label</span>
              <input
                placeholder="Item"
                value={newItem.label}
                onChange={e =>
                  setNewItem({ ...newItem, label: e.target.value })
                }
              />
            </label>
            <label>
              <span className="sr-only">Quantity</span>
              <input
                placeholder="Quantity"
                type="number"
                min={1}
                value={newItem.quantity}
                inputMode="numeric"
                onChange={e =>
                  setNewItem({ ...newItem, quantity: e.target.value })
                }
              />
            </label>
            <button type="submit" title="Add item">
              +<span className="sr-only">Add item</span>
            </button>
          </form>
          <div className="list">
            {groceryList}
            {isLoading && "Loading..."}
          </div>
        </div>
      </main>
    </div>
  );
};

export default App;
