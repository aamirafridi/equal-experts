import "@testing-library/jest-dom/extend-expect";

window.fetch = require("jest-fetch-mock");

(window.fetch as jest.Mock).mockResolvedValue({
  status: 200,
  ok: true,
  json: () => []
});
